package it.softwareengine.backup.backup;

import com.google.gson.JsonObject;
import it.softwareengine.backup.main.Main;
import it.softwareengine.backup.utility.Common;
import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.model.enums.AesKeyStrength;
import net.lingala.zip4j.model.enums.CompressionLevel;
import net.lingala.zip4j.model.enums.CompressionMethod;
import net.lingala.zip4j.model.enums.EncryptionMethod;
import org.apache.commons.lang3.Validate;

import java.io.File;
import java.io.IOException;
import java.security.CodeSource;

import static it.softwareengine.backup.init.Initializer.getCommon;

class Methods {

    void createBackup() {
        final CodeSource src = Common.class.getProtectionDomain().getCodeSource();
        final File jarFile = new File(src.getLocation().getPath());
        final String path = jarFile.getParentFile().getPath();

        final File backupsFolder = new File(path + "\\Backups");

        if (!backupsFolder.exists()) {
            Validate.isTrue(backupsFolder.mkdir());
        }

        final String savePath = path + "\\Backups\\backup_" + getCommon().getCurrentDate() + "_" + getCommon().getCurrentTime();

        final JsonObject obj = Main.getFile().getSettingsAsJsonObject();

        if (obj == null) {
            getCommon().showErrorMessage("Something went wrong while getting settings!");
            return;
        }

        final String host = obj.get("host").getAsString();
        final int port = obj.get("port").getAsInt();
        final String username = obj.get("username").getAsString();
        final String password = obj.get("password").getAsString();
        final String database = obj.get("database").getAsString();

        String os = "";

        if (getCommon().getOperatingSystem().toLowerCase().startsWith("windows")) {
            os = "cmd.exe /c ";
        } else if (getCommon().getOperatingSystem().toLowerCase().startsWith("linux")) {
            os = "sh -c ";
        }

        final String executeCmd = os +
                "mysqldump -h " + host
                + " -P " + port
                + " -u " + username
                + " -p" + password
                + " -f"
                + " -B " + database
                + " > " + savePath + ".sql";

        try {

            final Process process = Runtime.getRuntime().exec(executeCmd);
            final int processComplete = process.waitFor();

            if (processComplete != 0) {
                getCommon().sendNotification("Backup", "Backup failed.");
                return;
            }

        } catch (final InterruptedException | IOException e) {
            e.printStackTrace();
        }

        final File backup = new File(savePath + ".sql");

        if (!backup.exists()) {
            getCommon().showErrorMessage("Something went wrong: Backup file does not exists.");
            return;
        }

        createZip(backup, savePath + ".zip");
        Validate.isTrue(backup.delete());
    }

    private void createZip(final File file, final String path) {

        try {

            final ZipParameters parameters = new ZipParameters();

            parameters.setCompressionMethod(CompressionMethod.DEFLATE);
            parameters.setCompressionLevel(CompressionLevel.NORMAL);
            parameters.setEncryptFiles(true);
            parameters.setEncryptionMethod(EncryptionMethod.AES);
            parameters.setAesKeyStrength(AesKeyStrength.KEY_STRENGTH_256);

            new ZipFile(path, "ciaozio".toCharArray()).addFile(file, parameters);

        } catch (final IOException e) {
            getCommon().showErrorMessage("Something went wrong while zipping backup file!");
        }

    }

}
