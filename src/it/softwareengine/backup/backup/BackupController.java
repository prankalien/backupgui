package it.softwareengine.backup.backup;

import com.google.gson.JsonObject;
import it.softwareengine.backup.main.Main;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static it.softwareengine.backup.init.Initializer.getCommon;
import static it.softwareengine.backup.init.Initializer.getLoginController;

public class BackupController {

    private final Methods methods = new Methods();

    private boolean isBackupTaskStarted = false;
    private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

    @FXML
    private Button change;

    @FXML
    private void onStart() {

        if (isBackupTaskStarted) {
            getCommon().showErrorMessage("Program already started!");
            return;
        }

        final JsonObject obj = Main.getFile().getSettingsAsJsonObject();

        if (obj == null) {
            getCommon().showErrorMessage("Something went wrong while getting settings!");
            return;
        }

        isBackupTaskStarted = true;
        final int hours = obj.get("hours").getAsInt();

        getCommon().sendNotification("Backup", "Backup started.").showInformation();

        new Thread(() -> scheduler.scheduleAtFixedRate(() -> new Thread(methods::createBackup).start(),
                0,
                hours,
                TimeUnit.HOURS)).start();

    }

    @FXML
    private void onChangeSettings() {

        final Stage currentStage = (Stage) change.getScene().getWindow();
        currentStage.close();

        try {
            getLoginController().showLoginStage();
        } catch (final IOException e) {
            getCommon().showErrorMessage("Something went wrong while opening the backup stage...");
        }

    }

    public void showBackupStage() throws IOException {
        final Parent root = FXMLLoader.load(BackupController.class.getResource("../backup/Backup.fxml"));
        final Stage stage = new Stage();

        stage.setTitle("Backup");
        stage.setResizable(false);
        stage.getIcons().add(new Image("it/softwareengine/backup/resources/Icon.png"));
        stage.setIconified(false);
        stage.setScene(new Scene(root, 400, 200));
        stage.show();
    }

    public void setBackupTaskStarted(final boolean isBackupTaskStarted) {
        this.isBackupTaskStarted = isBackupTaskStarted;
    }

    public boolean isIsBackupTaskStarted() {
        return isBackupTaskStarted;
    }

    public ScheduledExecutorService getScheduler() {
        return scheduler;
    }

}
