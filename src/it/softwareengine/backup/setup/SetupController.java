package it.softwareengine.backup.setup;

import com.google.gson.JsonObject;
import it.softwareengine.backup.main.Main;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import static it.softwareengine.backup.init.Initializer.getBackupController;
import static it.softwareengine.backup.init.Initializer.getCommon;

public class SetupController {

    @FXML
    private TextField host, port, database, username, password;

    @FXML
    private void onSetup() {

        if (getCommon().areTextFieldsNull(host, port, database, username, password)) {
            getCommon().showErrorMessage("Fields cannot be empty!");
            return;
        }

        final String host = this.host.getText();
        final int port;

        try {
            port = Integer.parseInt(this.port.getText());
        } catch (final NumberFormatException ex) {
            getCommon().showErrorMessage("Field port must be numeric!");
            return;
        }

        final String database = this.database.getText();
        final String username = this.username.getText();
        final String password = this.password.getText();

        if (!getCommon().showConfirmDialog("Are fields correct?", null)) return;

        int hours;

        do {
            hours = getCommon().showInputIntegerDialog("Setup",
                    "Please insert after how many hours a backup should take place.",
                    "Hours:");
        } while (hours < 0);


        final JsonObject obj = new JsonObject();
        final JsonObject login = new JsonObject();

        obj.addProperty("host", host);
        obj.addProperty("port", port);
        obj.addProperty("username", username);
        obj.addProperty("password", password);
        obj.addProperty("database", database);
        obj.addProperty("hours", hours);

        login.addProperty("username", "admin");
        login.addProperty("password", "admin");

        obj.add("login", login);

        try (final FileWriter fileWriter = new FileWriter(Main.getFile().getSettingsFile())) {
            final BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write(obj.toString());
            bufferedWriter.flush();

        } catch (final IOException e) {
            getCommon().showErrorMessage("Something went wrong while writing the settings...");
            return;
        }

        final Stage stage = (Stage) this.host.getScene().getWindow();
        stage.close();

        try {
            getBackupController().showBackupStage();
        } catch (final IOException ex) {
            getCommon().showErrorMessage("Something went wrong while opening the backup stage...");
        }

    }

}

