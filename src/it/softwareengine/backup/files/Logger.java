package it.softwareengine.backup.files;

import org.apache.commons.lang3.Validate;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.CodeSource;

public class Logger {

    private static final CodeSource src = Settings.class.getProtectionDomain().getCodeSource();
    private static final File jarFile = new File(src.getLocation().getPath());
    private static final String path = jarFile.getParentFile().getPath();

    static {

        if (!new File(path + "/Logs").exists()) {
            Validate.isTrue(new File(path + "/Logs").mkdir());
        }

    }


    public static void createLogFile(final String name, final String... logs) {

        if (!new File(path + "/Logs").exists()) {
            Validate.isTrue(new File(path + "/Logs").mkdir());
        }

        final File file = new File(path + "/Logs/" + name + ".txt");

        try {
            Validate.isTrue(file.createNewFile());
        } catch (final IOException e) {
            e.printStackTrace();
        }

        if (file.exists()) {

            try (final FileWriter writer = new FileWriter(file);
                 final BufferedWriter bw = new BufferedWriter(writer)) {

                for (final String log : logs) {
                    bw.write(log);
                    bw.flush();
                }

            } catch (final IOException e) {
                System.err.format("IOException: %s%n", e);
            }

        }

    }

}
