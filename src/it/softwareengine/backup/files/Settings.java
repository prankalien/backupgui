package it.softwareengine.backup.files;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.lang3.Validate;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.security.CodeSource;

public class Settings {

    private static final CodeSource src = Settings.class.getProtectionDomain().getCodeSource();
    private static final File jarFile = new File(src.getLocation().getPath());
    private static final String path = jarFile.getParentFile().getPath();

    private static final File file = new File(path + "/Data/Settings.json");

    static {

        if (!new File(path + "/Data").exists()) {
            Validate.isTrue(new File(path + "/Data").mkdir());
        }

        if (!file.exists()) {

            try {
                Validate.isTrue(file.createNewFile());
            } catch (final IOException e) {
                e.printStackTrace();
            }

        }

    }

    public File getSettingsFile() {
        return file;
    }

    public JsonObject getSettingsAsJsonObject() {
        final JsonParser parser = new JsonParser();

        try (final FileReader reader = new FileReader(getSettingsFile())) {

            return parser.parse(reader).getAsJsonObject();

        } catch (final IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}
