package it.softwareengine.backup.login;

import com.google.gson.JsonObject;
import it.softwareengine.backup.main.Main;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

import static it.softwareengine.backup.init.Initializer.*;

public class LoginController {

    @FXML
    private TextField username, password;

    @FXML
    private void onLogin() {
        final String username = this.username.getText();
        final String password = this.password.getText();

        final JsonObject settings = Main.getFile().getSettingsAsJsonObject();
        final JsonObject login = settings.get("login").getAsJsonObject();

        final String settingsUsername = login.get("username").getAsString();
        final String settingsPassword = login.get("password").getAsString();

        if (!settingsUsername.equals(username) || !settingsPassword.equals(password)) {
            getCommon().showErrorMessage("Wrong username or password.");
            return;
        }

        final Stage loginStage = (Stage) this.username.getScene().getWindow();
        loginStage.close();

        try {
            getSettingsController().showSettingsStage();
        } catch (final IOException e) {
            getCommon().showErrorMessage("Something went wrong while opening the settings stage...");
        }

    }

    public void showLoginStage() throws IOException {
        final Parent root = FXMLLoader.load(LoginController.class.getResource("../login/Login.fxml"));
        final Stage stage = new Stage();

        stage.setTitle("Login");
        stage.setResizable(false);
        stage.getIcons().add(new Image("it/softwareengine/backup/resources/Icon.png"));
        stage.setIconified(false);

        stage.setOnCloseRequest(e -> {

            try {
                getBackupController().showBackupStage();
            } catch (final IOException ex) {
                getCommon().showErrorMessage("Something went wrong while opening the backup stage...");
            }

        });

        stage.setScene(new Scene(root, 435, 310));
        stage.show();
    }

}
