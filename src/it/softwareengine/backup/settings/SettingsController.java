package it.softwareengine.backup.settings;

import com.google.gson.JsonObject;
import it.softwareengine.backup.main.Main;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import static it.softwareengine.backup.init.Initializer.getBackupController;
import static it.softwareengine.backup.init.Initializer.getCommon;

public class SettingsController {

    @FXML
    private TextField host, port, database, username, password, hours, loginUsername, loginPassword;

    @FXML
    private void onComplete() {
        final JsonObject obj = Main.getFile().getSettingsAsJsonObject();

        if (obj == null) {

            try {
                getBackupController().showBackupStage();
            } catch (final IOException e) {
                getCommon().showErrorMessage("Something went wrong while opening the backup stage...");
            }

            return;
        }

        host.setText(obj.get("host").getAsString());
        port.setText(obj.get("port").getAsString());
        database.setText(obj.get("database").getAsString());
        username.setText(obj.get("username").getAsString());
        password.setText(obj.get("password").getAsString());
        hours.setText(obj.get("hours").getAsString());

        final JsonObject login = obj.get("login").getAsJsonObject();

        loginUsername.setText(login.get("username").getAsString());
        loginPassword.setText(login.get("password").getAsString());
    }

    @FXML
    private void onSave() {

        if (getCommon().areTextFieldsNull(host, port, database, username, password, loginPassword, loginUsername, hours)) {
            getCommon().showErrorMessage("Fields cannot be empty!");
            return;
        }

        if (!getCommon().showConfirmDialog("Do you really want to change the settings?",
                "*WARNING* If a scheduler is running, it will be stopped.")) return;

        if (!getBackupController().getScheduler().isTerminated()) {
            getBackupController().getScheduler().shutdown();
        }

        final String host = this.host.getText();
        final int port;
        final int hours;

        try {
            port = Integer.parseInt(this.port.getText());
            hours = Integer.parseInt(this.hours.getText());
        } catch (final NumberFormatException ex) {
            getCommon().showErrorMessage("Field port or hours must be numeric!");
            return;
        }

        final String database = this.database.getText();
        final String username = this.username.getText();
        final String password = this.password.getText();
        final String loginUsername = this.loginUsername.getText();
        final String loginPassword = this.loginPassword.getText();

        final JsonObject obj = new JsonObject();
        final JsonObject login = new JsonObject();

        obj.addProperty("host", host);
        obj.addProperty("port", port);
        obj.addProperty("username", username);
        obj.addProperty("password", password);
        obj.addProperty("database", database);
        obj.addProperty("hours", hours);

        login.addProperty("username", loginUsername);
        login.addProperty("password", loginPassword);

        obj.add("login", login);

        try (final FileWriter file = new FileWriter(Main.getFile().getSettingsFile())) {
            final BufferedWriter bw = new BufferedWriter(file);

            bw.write(obj.toString());
            bw.flush();

        } catch (final IOException e) {

            getCommon().showErrorMessage("Something went wrong while writing a file...");

        } finally {

            getBackupController().setBackupTaskStarted(false);

            final Stage settingsStage = (Stage) this.host.getScene().getWindow();
            settingsStage.close();

            try {
                getBackupController().showBackupStage();
            } catch (final IOException e) {
                getCommon().showErrorMessage("Something went wrong while opening the backup window...");
            }

        }

    }

    public void showSettingsStage() throws IOException {
        final Parent root = FXMLLoader.load(getClass().getResource("Settings.fxml"));
        final Stage stage = new Stage();

        stage.setTitle("Settings");
        stage.setResizable(false);
        stage.getIcons().add(new Image("it/softwareengine/backup/resources/Icon.png"));
        stage.setIconified(false);

        stage.setOnCloseRequest(e -> {

            try {
                getBackupController().showBackupStage();
            } catch (final IOException ex) {
                getCommon().showErrorMessage("Something went wrong while opening the backup stage...");
            }

        });

        stage.setScene(new Scene(root, 600, 497));
        stage.show();
    }

}
