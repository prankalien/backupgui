package it.softwareengine.backup.main;

import it.softwareengine.backup.backup.BackupController;
import it.softwareengine.backup.files.Logger;
import it.softwareengine.backup.files.Settings;
import it.softwareengine.backup.setup.SetupController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class Main extends Application {

    private static Settings file;
    private static Logger log;

    @Override
    public void start(final Stage primaryStage) throws IOException {

        final Parent root;
        final String title;
        final int height, width;

        if (file.getSettingsFile().length() == 0) {
            root = FXMLLoader.load(SetupController.class.getResource("Setup.fxml"));
            title = "Setup";
            height = 350;
            width = 600;
        } else {
            root = FXMLLoader.load(BackupController.class.getResource("Backup.fxml"));
            title = "Backup";
            height = 200;
            width = 400;
        }

        primaryStage.setTitle(title);
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image("it/softwareengine/backup/resources/Icon.png"));
        primaryStage.setIconified(false);
        primaryStage.setScene(new Scene(root, width, height));
        primaryStage.show();
    }

    public static void main(final String[] args) {
        log = new Logger();
        file = new Settings();

        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                System.out.println(format(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory(), 2));
            }
        }, 0, 1000);

        launch(args);
    }

    public static Settings getFile() {
        return file;
    }

    public static Logger getLog() {
        return log;
    }

    private static String format(double bytes, final int digits) {
        final String[] dictionary = {"bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"};
        int index = 0;
        for (index = 0; index < dictionary.length; index++) {
            if (bytes < 1024) {
                break;
            }
            bytes = bytes / 1024;
        }
        return String.format("%." + digits + "f", bytes) + " " + dictionary[index];
    }

}
