package it.softwareengine.backup.init;

import it.softwareengine.backup.backup.BackupController;
import it.softwareengine.backup.login.LoginController;
import it.softwareengine.backup.settings.SettingsController;
import it.softwareengine.backup.utility.Common;

public class Initializer {

    private static final BackupController backupController = new BackupController();
    private static final LoginController loginController = new LoginController();
    private static final SettingsController settingsController = new SettingsController();
    private static final Common common = new Common();

    public static BackupController getBackupController() {
        return backupController;
    }

    public static LoginController getLoginController() {
        return loginController;
    }

    public static SettingsController getSettingsController() {
        return settingsController;
    }

    public static Common getCommon() {
        return common;
    }

}
