package it.softwareengine.backup.utility;

import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.controlsfx.control.Notifications;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import static javafx.scene.control.Alert.AlertType;

public class Common {

    public boolean areTextFieldsNull(final TextField... fields) {

        for (final TextField field : fields) {

            if (field.getText().equals("")) {
                return true;
            }

        }

        return false;
    }

    public void showErrorMessage(final String errorMessage) {
        final Alert alert = new Alert(AlertType.ERROR);

        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText(errorMessage);
        alert.setResizable(false);

        final Stage a = (Stage) alert.getDialogPane().getScene().getWindow();

        a.getIcons().add(new Image("it/softwareengine/backup/resources/Icon.png"));

        alert.showAndWait();
    }

    public boolean showConfirmDialog(final String question, final String header) {
        final Alert alert = new Alert(AlertType.CONFIRMATION, question, ButtonType.YES, ButtonType.NO);

        alert.setTitle("Confirm");
        alert.setHeaderText(header);
        alert.setResizable(false);

        final Stage a = (Stage) alert.getDialogPane().getScene().getWindow();

        a.getIcons().add(new Image("it/softwareengine/backup/resources/Icon.png"));

        alert.showAndWait();

        return alert.getResult() == ButtonType.YES;
    }

    public String getCurrentTime() {
        final DateFormat dateFormat = new SimpleDateFormat("HH_mm_ss");
        final Date date = new Date();

        return dateFormat.format(date);
    }

    public String getCurrentDate() {
        final DateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy");
        final Date date = new Date();

        return dateFormat.format(date);
    }

    public Notifications sendNotification(final String title, final String text) {
        return Notifications.create()
                .graphic(null)
                .title(title)
                .text(text)
                .hideCloseButton()
                .position(Pos.TOP_RIGHT);
    }

    public String getOperatingSystem() {
        return System.getProperty("os.name");
    }

    public int showInputIntegerDialog(final String title, final String header, final String text) {

        final TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle(title);
        dialog.setHeaderText(header);
        dialog.setContentText(text);
        dialog.setResizable(false);

        final Stage a = (Stage) dialog.getDialogPane().getScene().getWindow();

        a.getIcons().add(new Image("it/softwareengine/backup/resources/Icon.png"));

        final Optional<String> result = dialog.showAndWait();

        if (!result.isPresent()) {
            return -1;
        }

        int n = -1;

        try {
            n = Integer.parseInt(result.get());
        } catch (final NumberFormatException e) {
            showErrorMessage("Please enter a number value.");
        }

        return n;
    }

}
